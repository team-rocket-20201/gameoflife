
const size=39;
const grid=createEmptyGrid();
fillGrid(grid);

export const getGrid = () => {
    return Object.values(grid);
}

export const task = async () => {
    let n=0;
    while(n<10){
      await new Promise(r => setTimeout(r, 1000));
      applyChanges(grid);
      window.location.reload(false); 
      n++;
    }
  }

export function exec() {
    let n=0;
    while(n<100){
        for(let i=0; i<size; i++){
            console.log(grid[i][0].alive+" "+grid[i][1].alive+" "+grid[i][2].alive+" "+grid[i][3].alive+" "+grid[i][4].alive);
        }
        console.log('postChange');
        applyChanges(grid);
        n++;
    }
}


function createEmptyGrid(){
    let grid=new Array(size);
    for (let i = 0; i <size; i++) { 
        grid[i] = new Array(size); 
    }
    return grid;
}


function fillGrid(grid){
    for(let i=0; i<size; i++){
        for(let j=0; j<size; j++){
            grid[i][j] = new Cell(i, j);
        }
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function setAliveRandom() {
    if(getRandomInt(2)==0){
        return false;
    } else{
        return true;
    }
}

function Cell(row, column) {
    this.id = row*size+column;
    this.row = row;
    this.column = column;
    this.alive = setAliveRandom();
    this.nextStateCell = nextStateCell;
}

function nextState(){
    let toChange = new Array(size*size);
    for(let i=0; i<size; i++){
        for(let j=0; j<size; j++){
            toChange.push(grid[i][j].nextStateCell());
        }
    }
    return toChange;
}

function nextStateCell(){
    let alive=0;
    if(this.row>0){
        if(this.column>0){
            alive += (grid[this.row-1][this.column-1].alive == true) ? 1 : 0;
        }
        alive += (grid[this.row-1][this.column].alive == true) ? 1 : 0;
        if(this.column<size-1){
            alive += (grid[this.row-1][this.column+1].alive == true) ? 1 : 0;
        }
    }
    if(this.column>0){
        alive += (grid[this.row][this.column-1].alive == true) ? 1 : 0;
    }
    if(this.column< size-1){
        alive += (grid[this.row][this.column+1].alive == true) ? 1 : 0;
    }
    if(this.row<size-1){
        if(this.column>0){
            alive += (grid[this.row+1][this.column-1].alive == true) ? 1 : 0;
        }
        alive += (grid[this.row+1][this.column].alive == true) ? 1 : 0;
        if(this.column<size-1){
            alive += (grid[this.row+1][this.column+1].alive == true) ? 1 : 0;
        }
    }
    if(this.alive==true && alive<2){
        return new Array(this.row, this.column);
    }
    else if(this.alive==true && alive>3){
        return new Array(this.row, this.column);
    }
    else if(this.alive==false && alive >2){
        return new Array(this.row, this.column);
    }
    else {
        return null;
    }
}

function applyChanges(grid){
    let stillMoving = false;
    let toChange = nextState();
    for(let i=0; i<toChange.length; i++){
        if(toChange[i]!= null) {
            stillMoving = true;
            grid[toChange[i][0]][toChange[i][1]].alive = !grid[toChange[i][0]][toChange[i][1]].alive;
        }
    }
    if(stillMoving==false){
        console.log("not moving anymore ...")
    }
}
