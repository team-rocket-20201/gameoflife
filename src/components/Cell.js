import React, { useState } from 'react';

export default function Cell({alive}){
    const [value, setValue] = useState();

    const refresh = ()=>{
        // re-renders the component
        setValue({});
    }

    if(alive){
        return(
            <div className="cell"
            width="30px"
            height="30px"
            style={{background:'#33D2FF', float:'left', width:'30px', height:'30px', border:'1px solid black'}}>
            </div>
        );
    }
    else{
        return(
            <div className="cell"
            width="30px"
            height="30px"
            style={{background:'black', float:'left', width:'30px', height:'30px', border:'1px solid black'}}>
            </div>
        );
    }
    
}
